package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class testsClientes {
	
	private GestorContabilidad gc;
	
	@BeforeEach
	public  void inicializar() {
		gc = new GestorContabilidad();
		Cliente cliente = new Cliente("C1", LocalDate.now(), "1111A");
		Cliente cliente1 = new Cliente("C2", LocalDate.now(), "2222B");
		Cliente cliente2 = new Cliente("C3", LocalDate.now(), "3333C");
		gc.altaCliente(cliente);
		gc.altaCliente(cliente1);
		gc.altaCliente(cliente2);
		Factura factura = new Factura("F1", LocalDate.now(), "Juego", 2.0, 2, cliente);
		Factura factura1 = new Factura("F2", LocalDate.now(), "Juego2", 3.0, 3, cliente1);
		Factura factura2 = new Factura("F3", LocalDate.now(), "Juego3", 4.0, 4, cliente2);
		gc.crearFactura(factura);
		gc.crearFactura(factura1);
		gc.crearFactura(factura2);
	}
	
	@Test
	void buscarClienteExistente() {
		assertNotNull(gc.buscarCliente("C1"), "No encuentra un cliente existente");
	}
	
	@Test
	void buscarClienteInexistente() {
		assertNull(gc.buscarCliente("F40"), "No devuelve null en una busqueda inexistente");
		gc.eliminarCliente("C1");
		assertNull(gc.buscarCliente("C1"), "Encuentra un cliente eliminada");
		//ver pruebas de borrado
	}

	@Test
	void crearAltaNueva() {
		Cliente x = new Cliente("miau", null, "chevere");
		gc.altaCliente(x);
		assertTrue(gc.getListaClientes().contains(x), "No introduce clientes nuevos");		
	}
	
	@Test
	void crearAltaExistente() {
		int asdf = gc.getListaClientes().size();
		Cliente x = new Cliente("C1", null, "chevere");
		gc.altaCliente(x);
		
		assertEquals(asdf,gc.getListaFacturas().size(), "Introduce clientes con mismo codigo");
		assertFalse(gc.getListaClientes().contains(x), "Sobreescribe clientes con mismo codigo");		
	}
	
	@Test
	void borrarClienteExistente() {
		gc.eliminarCliente("C1");
		assertNull(gc.buscarCliente("C1"), "Encuentra un cliente eliminado");
	}
	
	@Test
	void borrarYReintroducir() {
		gc.eliminarCliente("C1");
		assertNull(gc.buscarCliente("C1"), "Encuentra una factura eliminada");
		gc.altaCliente(new Cliente("C1", null, "chevere"));
		assertNotNull(gc.buscarCliente("C1"), "No permite reutilizar codigos si se borra un cliente");
	}
	
	@Test
	@Disabled
	void testClienteMasAntiguoConClientes() {
		LocalDate anterior = LocalDate.of(3000, 3, 2);
		gc.altaCliente(new Cliente("C1", LocalDate.of(3000, 12, 3), "chevere"));
		LocalDate posterior = gc.clienteMasAntiguo().getFechaAlta();
		assertTrue(posterior.isAfter(anterior) , "No devuelve el cliente mas mas antiguo");
	}
	
	@Test
	void testFacturaMasCaraSinFacturas() {
		gc = new GestorContabilidad();
		assertNull(gc.facturaMasCara(), "No devuelve null");
	}
}
