package tests;
import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class testsFacturas {
	private GestorContabilidad gc;
	
	@BeforeEach
	public  void inicializar() {
		gc = new GestorContabilidad();
		Cliente cliente = new Cliente("C1", LocalDate.now(), "1111A");
		Cliente cliente1 = new Cliente("C2", LocalDate.now(), "2222B");
		Cliente cliente2 = new Cliente("C3", LocalDate.now(), "3333C");
		gc.altaCliente(cliente);
		gc.altaCliente(cliente1);
		gc.altaCliente(cliente2);
		Factura factura = new Factura("F1", LocalDate.now(), "Juego", 2.0, 2, cliente);
		Factura factura1 = new Factura("F2", LocalDate.now(), "Juego2", 3.0, 3, cliente1);
		Factura factura2 = new Factura("F3", LocalDate.now(), "Juego3", 4.0, 4, cliente2);
		gc.crearFactura(factura);
		gc.crearFactura(factura1);
		gc.crearFactura(factura2);
	}
	
	
	@Test
	void buscarFacturaExistente() {
		assertFalse(gc.buscarFactura("F1") == null, "No encuentra una factura existente");
	}
	
	@Test
	void buscarFacturaInexistente() {
		assertTrue(gc.buscarFactura("F40") == null, "No devuelve null en una busqueda inexistente");
		gc.eliminarFactura("F1");
		assertTrue(gc.buscarFactura("F1") == null, "Encuentra una factura eliminada");
		//ver pruebas de borrado
	}
	
	@Test
	void crearFacturaNueva() {
		Factura x = new Factura("1234", LocalDate.now(), "Cosa", 1.2, 11, null);
		gc.crearFactura(x);
		assertTrue(gc.getListaFacturas().contains(x), "No introduce facturas nuevas");		
	}
	
	@Test
	void crearFacturaExistente() {
		int asdf = gc.getListaFacturas().size();
		Factura x = new Factura("F1", LocalDate.now(), "Cosa", 1.2, 11, null);
		gc.crearFactura(x);
			
		assertEquals(asdf,gc.getListaFacturas().size(), "Introduce facturas con mismo codigo");
		assertFalse(gc.getListaFacturas().contains(x), "Sobreescribe facturas con mismo codigo");
	}
	
	@Test
	void borrarFacturaExistente() {
		gc.eliminarFactura("F1");
		assertNull(gc.buscarFactura("F1"), "Encuentra una factura eliminada");
	}
	
	@Test
	void borrarYReintroducir() {
		gc.eliminarFactura("F1");
		assertTrue(gc.buscarFactura("F1") == null, "Encuentra una factura eliminada");
		gc.crearFactura(new Factura("F1", null, null, 0, 0, null));
		assertNotNull(gc.buscarFactura("F1"), "No permite reutilizar codigos si se borra una factura");
	}
	
	@Test
	void testFacturaMasCaraConFacturas() {
		gc.crearFactura(new Factura("asdfa", LocalDate.now(), "nya", 123.4, 1, null));
		double aux = gc.facturaMasCara().calcularPrecioFactura();
		assertEquals(aux, 123.4, "No devuelve la factura mas cara");
	}
	
	@Test
	void testFacturaMasCaraSinFacturas() {
		gc = new GestorContabilidad();
		assertNull(gc.facturaMasCara(), "No devuelve null");
	}
	
	@Test
	void facturaAnualAnyoConInfo() {
		double aux = gc.calcularFacturacionAnual(LocalDate.now().getYear());
		double precioTotal = 29.0;
		assertEquals(aux, precioTotal, "No calcula correctamente");
	}
	
	@Test
	void facturaAnualAnyoSinInfo() {
		double aux = gc.calcularFacturacionAnual(LocalDate.now().getYear()-1);
		double precioTotal = 0.0;
		assertEquals(aux, precioTotal, "No calcula correctamente");
	}
	
	@Test
	void facturaAnualSinFacturas() {
		gc = new GestorContabilidad();
		double aux = gc.calcularFacturacionAnual(LocalDate.now().getYear());
		double precioTotal = 0.0;
		assertEquals(aux, precioTotal, "No calcula correctamente");
	}
	
	@Test
	void asignarClienteExistente() {
		gc.crearFactura(new Factura("F5", LocalDate.now(), "nyaa", 42, 42, null));
		gc.asignarClienteAFactura("C1", "F5");
		assertNotNull(gc.buscarFactura("F5").getCliente(), "No modifica el cliente");
	}
	
	@Test
	void asignarClienteNoExistente() {
		gc.crearFactura(new Factura("F5", LocalDate.now(), "nyaa", 42, 42, null));
		gc.asignarClienteAFactura("inexistente", "F5");
		assertNull(gc.buscarFactura("F5").getCliente(), "Modifica con un cliente no existente");
	}
	
	@Test
	void cantidadFacturasClienteSinFacturas() {
		gc.altaCliente(new Cliente("C13", LocalDate.now(), "nya"));
		int esperado = 0;
		int obtenido = gc.cantidadFacturasPorCliente("C13");
		assertEquals(obtenido,esperado, "No cuenta bien a usuarios sin facturas");
	}
	
	@Test
	void cantidadFacturasClienteConFacturas() {
		int esperado = 1;
		int obtenido = gc.cantidadFacturasPorCliente("C1");
		assertEquals(obtenido, esperado, "No cuenta bien a usuarios con facturas");
	}
	
	@Test
	void cantidadFacturasClienteInexistente() {
		int esperado = 0;
		int obtenido = gc.cantidadFacturasPorCliente("miau");
		assertEquals(obtenido, esperado, "No cuenta bien si el usuario no existe");
	}
}
