package clases;

import java.util.ArrayList;

public class GestorContabilidad {

	private ArrayList<Factura> listaFacturas;
	private ArrayList<Cliente> listaClientes;

	public GestorContabilidad() {
		listaFacturas = new ArrayList<>();
		listaClientes = new ArrayList<>();
	}

	public Cliente buscarCliente(String dni) {
		Cliente aux = null;
		for (Cliente cliente : listaClientes) {
			if (cliente.getDni().equals(dni)) {
				aux = cliente;
				break;
			}
		}
		return aux;
	}

	public Factura buscarFactura(String codigo) {
		Factura aux = null;
		for (Factura factura : listaFacturas) {
			if (factura.getCodigoFactura().equals(codigo)) {
				aux = factura;
				break;
			}
		}
		return aux;
	}

	public void altaCliente(Cliente cliente) {
		boolean existe = false;
		if (listaClientes.isEmpty()) {
			listaClientes.add(cliente);
		} else {
			for (Cliente cliente1 : listaClientes) {
				if (cliente.getDni().equals(cliente1.getDni())) {
					existe = true;
					break;
				}
			}
			if (!existe) {
				listaClientes.add(cliente);
			}
		}

	}

	public void crearFactura(Factura factura) {
		boolean existe = false;
		if (listaFacturas != null) {
			for (Factura factura1 : listaFacturas) {
				if (factura.getCodigoFactura().equals(factura1.getCodigoFactura())) {
					existe = true;
					break;
				}
			}
			if (!existe) {
				listaFacturas.add(factura);
			}
		}
	}

	public Cliente clienteMasAntiguo() {
		if (listaClientes != null && !listaClientes.isEmpty()) {
			Cliente clienteMasAntiguo = listaClientes.get(0);
			for (Cliente cliente : listaClientes) {
				if (cliente.getFechaAlta().isAfter(clienteMasAntiguo.getFechaAlta())) {
					clienteMasAntiguo = cliente;
				}
			}
			return clienteMasAntiguo;
		}
		return null;
	}

	public Factura facturaMasCara() {
		if (listaFacturas != null && !listaFacturas.isEmpty()) {
			Factura facturaMasCara = listaFacturas.get(0);
			for (Factura factura : listaFacturas) {
				if (facturaMasCara.calcularPrecioFactura() < factura.calcularPrecioFactura()) {
					facturaMasCara = factura;
				}
			}
			return facturaMasCara;
		}
		return null;
	}

	public double calcularFacturacionAnual(int anyo) {
		if (listaFacturas != null) {
			double precio = 0;
			for (Factura factura : listaFacturas) {
				if (factura.getFecha().getYear() == anyo) {
					precio += factura.calcularPrecioFactura();
				}
			}
			return precio;
		}
		return 0;
	}

	public void asignarClienteAFactura(String dni, String codigoFactura) {
		Cliente cliente = buscarCliente(dni);
		Factura factura = buscarFactura(codigoFactura);
		if (cliente != null && factura != null) {
			factura.setCliente(cliente);
		}
	}

	public int cantidadFacturasPorCliente(String dni) {
		Cliente cliente = buscarCliente(dni);
		int cantidad = 0;
		if (cliente != null) {
			for (Factura factura : listaFacturas) {
				if (factura.getCliente().getDni().equals(cliente.getDni())) {
					cantidad++;
				}
			}
			return cantidad;
		}
		return 0;
	}

	public void eliminarFactura(String codigo) {
		listaFacturas.remove(buscarFactura(codigo));
	}

	public void eliminarCliente(String dni) {
		listaClientes.remove(buscarCliente(dni));
	}

	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}

	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}

}