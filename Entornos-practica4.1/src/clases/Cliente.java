package clases;

import java.time.LocalDate;

public class Cliente {
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDni() {
		return dni;
	}
	public void setDni(String dni) {
		this.dni = dni;
	}
	public LocalDate getFechaAlta() {
		return fechaAlta;
	}
	public void setFechaAlta(LocalDate fechaAlta) {
		this.fechaAlta = fechaAlta;
	}
	private String dni;
	private LocalDate fechaAlta;
	private String nombre;
	public Cliente(String dni, LocalDate fechaAlta, String nombre) {
		super();
		this.dni = dni;
		this.fechaAlta = fechaAlta;
		this.nombre = nombre;
	}
}
